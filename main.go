package main

import (
	"fmt"
	"html"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/logthis"
	"gitlab.com/passelecasque/obstruction/torrent"
	"gitlab.com/passelecasque/obstruction/tracker"
)

const (
	DefaultConfigurationFile = "config.yaml"
)

var (
	excludedSubDirs = []string{"Metadata", "TrackerMetadata"}
)

func main() {
	// parsing CLI
	cli := &pollinatorArgs{}
	if err := cli.parseCLI(os.Args[1:]); err != nil {
		logthis.Error(err, logthis.NORMAL)
		return
	}
	if cli.builtin {
		return
	}

	logthis.TimedInfo("Pollinator! Setting up.", logthis.VERBOSE)
	// loading configuration
	logthis.TimedInfo("Loading configuration.", logthis.VERBOSE)
	conf, err := NewConfig(DefaultConfigurationFile)
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
		return
	}

	// dealing with show-config command
	if cli.showConfig {
		fmt.Println(conf.String())
		return
	}

	logthis.TimedInfo("Logging in the trackers.", logthis.NORMAL)
	originTrackerConfig, err := config.GetTracker(cli.trackerOrigin)
	if err != nil {
		logthis.TimedInfo("unknown origin tracker: "+cli.trackerOrigin, logthis.NORMAL)
		return
	}
	originTracker, err := tracker.NewGazelle(originTrackerConfig.Name, originTrackerConfig.URL, originTrackerConfig.User, originTrackerConfig.Password, "session", originTrackerConfig.Cookie, userAgent())
	if err != nil {
		logthis.TimedInfo("Error setting up tracker "+originTrackerConfig.Name+": "+err.Error(), logthis.NORMAL)
		return
	}
	go originTracker.RateLimiter()
	if err := originTracker.Login(); err != nil {
		logthis.TimedInfo("Error logging in tracker "+originTrackerConfig.Name+": "+err.Error(), logthis.NORMAL)
		return
	}

	destinationTrackerConfig, err := config.GetTracker(cli.trackerDestination)
	if err != nil {
		logthis.TimedInfo("unknown destination tracker: "+cli.trackerDestination, logthis.NORMAL)
		return
	}
	destinationTracker, err := tracker.NewGazelle(destinationTrackerConfig.Name, destinationTrackerConfig.URL, destinationTrackerConfig.User, destinationTrackerConfig.Password, "session", destinationTrackerConfig.Cookie, userAgent())
	if err != nil {
		logthis.TimedInfo("Error setting up tracker "+destinationTrackerConfig.Name+": "+err.Error(), logthis.NORMAL)
		return
	}
	go destinationTracker.RateLimiter()
	if err := destinationTracker.Login(); err != nil {
		logthis.TimedInfo("Error logging in tracker "+destinationTracker.Name+": "+err.Error(), logthis.NORMAL)
		return
	}
	if err := destinationTracker.GetAnnounceURL(); err != nil {
		logthis.TimedInfo("Error getting announce URL from tracker "+destinationTracker.Name+": "+err.Error(), logthis.NORMAL)
		return
	}

	logthis.TimedInfo("Getting information from the origin tracker.", logthis.NORMAL)
	var gzTorrent *tracker.GazelleTorrent
	if cli.torrentPath != "" {
		gzTorrent, err = originTracker.GetTorrentFromFile(cli.torrentPath)
	} else if cli.torrentID != 0 {
		gzTorrent, err = originTracker.GetTorrent(cli.torrentID)
	} else {
		logthis.TimedInfo("No source to get information from origin tracker.", logthis.NORMAL)
		return
	}
	if err != nil {
		logthis.TimedInfo("Error getting torrent info: "+err.Error(), logthis.NORMAL)
		return
	}

	logthis.TimedInfo("Looking for local files.", logthis.NORMAL)
	downloadedDirectory := filepath.Join(config.Folders.DownloadDir, html.UnescapeString(gzTorrent.Response.Torrent.FilePath))
	if !fs.DirExists(downloadedDirectory) {
		logthis.TimedInfo("Downloaded files for this release not found: "+html.UnescapeString(gzTorrent.Response.Torrent.FilePath), logthis.NORMAL)
		return
	}

	// TODO CHECK FILE COUNT + FILE LIST!!!!
	// fmt.Println(gzTorrent.Response.Torrent.FileCount)
	// fmt.Println(gzTorrent.Response.Torrent.FileList)
	// TODO make other checks???

	logthis.TimedInfo("Generating torrent with "+destinationTracker.Name+" announce URL.", logthis.NORMAL)
	destinationTorrent := torrent.New(downloadedDirectory)
	destinationTorrentFilename := fmt.Sprintf("pollinator_%s_%s_%d.torrent", cli.trackerOrigin, cli.trackerDestination, cli.torrentID)
	if err := destinationTorrent.Generate(destinationTorrentFilename, "", strings.ToUpper(destinationTrackerConfig.Name), []string{destinationTracker.Passkey}, fmt.Sprintf(fullVersion, fullName, Version), excludedSubDirs); err != nil {
		logthis.TimedInfo("Could not generate .torrent: "+err.Error(), logthis.NORMAL)
		return
	}

	logthis.TimedInfo("Trying to locate an existing torrent group on "+destinationTracker.Name+".", logthis.NORMAL)
	var mainArtists []string
	for _, a := range gzTorrent.Response.Group.MusicInfo.Artists {
		mainArtists = append(mainArtists, a.Name)
	}
	results, err := destinationTracker.Search(strings.Join(mainArtists, " "), gzTorrent.Response.Group.Name)
	if err != nil {
		logthis.TimedInfo("Error searching for groups: "+err.Error(), logthis.NORMAL)
		return
	}

	var existingGroupID int
	if len(results.Response.Results) != 0 {
		// NOTE: checking for exact artist name + release title + release type
		// Since metadata may follow different conventions, this might hide some results.
		// At worst, some group merging will have to be done post-upload.
		for _, res := range results.Response.Results {
			sameTitle := strings.EqualFold(gzTorrent.Response.Group.Name, res.GroupName)
			sameArtist := strings.EqualFold(strings.Join(mainArtists, " & "), res.Artist)
			sameReleaseType := tracker.GazelleReleaseType(gzTorrent.Response.Group.ReleaseType) == res.ReleaseType
			if sameTitle && sameArtist && sameReleaseType {
				existingGroupID = res.GroupID
				break
			}
		}
	}
	if existingGroupID != 0 {
		logthis.TimedInfo("Found existing group: "+fmt.Sprintf("%s/torrents.php?id=%d", destinationTrackerConfig.URL, existingGroupID), logthis.NORMAL)
	} else {
		logthis.TimedInfo("No existing group found.", logthis.NORMAL)
	}

	logthis.TimedInfo("Updating metadata.", logthis.NORMAL)
	gzTorrent.Response.Torrent.Description += fmt.Sprintf("\n\nCross-posted from [b]%s[/b]", strings.ToUpper(originTracker.Name))
	if existingGroupID == 0 {
		logthis.TimedInfo("Retrieving original group bbcode description.", logthis.NORMAL)
		// gazelle API does not return bbcode for group description (wikibody)
		// as a workaround, parsing the group edit page to get the bbcode
		originalDescription, _, _, err := originTracker.GetGroupEditInformation(gzTorrent.Response.Group.ID)
		if err != nil {
			logthis.TimedInfo("Error getting bbcode description: "+err.Error(), logthis.NORMAL)
			return
		}
		gzTorrent.Response.Group.WikiBody = originalDescription
	}
	// if no group, blanking the field; else setting the target group ID
	gzTorrent.Response.Group.ID = existingGroupID

	logthis.TimedInfo("Uploading.", logthis.NORMAL)
	var uploadedGroupID int
	var uploadErr error
	if existingGroupID != 0 {
		uploadedGroupID, uploadErr = destinationTracker.UploadMusicToExistingGroup(existingGroupID, destinationTorrentFilename, []string{}, gzTorrent)
	} else {
		uploadedGroupID, uploadErr = destinationTracker.UploadMusic(destinationTorrentFilename, []string{}, gzTorrent)
	}
	if uploadErr != nil {
		logthis.TimedInfo("Error uploading: "+uploadErr.Error(), logthis.NORMAL)
		return
	}
	logthis.TimedInfo(fmt.Sprintf("Torrent was uploaded to group %s", fmt.Sprintf("%s/torrents.php?id=%d", destinationTrackerConfig.URL, uploadedGroupID)), logthis.NORMAL)

	// getting the new torrentID
	torrentID, err := destinationTracker.GetLastUploadedToTorrentGroup(uploadedGroupID)
	if err != nil {
		logthis.TimedInfo("Error looking for torrent ID: "+err.Error(), logthis.NORMAL)
		return
	}
	logthis.TimedInfo(fmt.Sprintf("Torrent was uploaded to %s", fmt.Sprintf("%s/torrents.php?torrentid=%d", destinationTrackerConfig.URL, torrentID)), logthis.NORMAL)

	logthis.TimedInfo("Starting to seed.", logthis.NORMAL)
	if err := fs.CopyFile(destinationTorrentFilename, filepath.Join(config.Folders.WatchDir, filepath.Base(destinationTorrentFilename)), false); err != nil {
		logthis.TimedInfo("Error copying .torrent to watch directory: "+err.Error(), logthis.NORMAL)
		return
	}
	if err := os.Remove(destinationTorrentFilename); err != nil {
		logthis.TimedInfo("Error removing .torrent from current directory: "+err.Error(), logthis.NORMAL)
		return
	}
	logthis.TimedInfo("Done.", logthis.NORMAL)
}
