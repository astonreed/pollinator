module gitlab.com/passelecasque/pollinator

go 1.13

require (
	github.com/docopt/docopt-go v0.0.0-20180111231733-ee0de3bc6815
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.4.0
	gitlab.com/catastrophic/assistance v0.29.0
	gitlab.com/passelecasque/obstruction v0.7.7
	gopkg.in/yaml.v2 v2.2.8
)

//replace gitlab.com/passelecasque/obstruction => ../../passelecasque/obstruction
